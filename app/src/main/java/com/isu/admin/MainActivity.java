package com.isu.admin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    ArrayList<AdminItems> adminItems;
    JSONArray jsonArray;
    AdminAdapter adapter;
    RecyclerView recyclerView;

    ProgressDialog dialog;

    TextInputEditText packageEt, adminET, appCodeET, appNameET, createdET, mainAppET, uniqueET, versionET;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String s = loadJson(MainActivity.this);
        recyclerView = findViewById(R.id.main_rv);

        packageEt = findViewById(R.id.home_package);
        adminET = findViewById(R.id.home_admin);
        appCodeET = findViewById(R.id.home_code);
        appNameET = findViewById(R.id.home_name);
        createdET = findViewById(R.id.home_created);
        mainAppET = findViewById(R.id.home_main);
        uniqueET = findViewById(R.id.home_unique);
        versionET = findViewById(R.id.home_version);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        adminItems = new ArrayList<>();
        jsonArray = new JSONArray();
        try {
            jsonArray = new JSONArray(s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        adminItems = convert(jsonArray);

        adapter = new AdminAdapter(MainActivity.this, adminItems);
        recyclerView.setAdapter(adapter);

        findViewById(R.id.home_add_manually).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.cv_add_new).setVisibility(View.VISIBLE);
                findViewById(R.id.home_add).setVisibility(View.VISIBLE);
                findViewById(R.id.home_add_manually).setVisibility(View.GONE);
            }
        });

        findViewById(R.id.main_click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFirestore(adminItems);
            }
        });
        findViewById(R.id.show_list).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, AdminListActivity.class));
            }
        });

        findViewById(R.id.home_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new ProgressDialog(MainActivity.this);
                dialog.setMessage("Loading...");
                dialog.setCancelable(false);
                dialog.show();

                Map<String, Object> appDetails = new HashMap<>();
                appDetails.put("AdminName", adminET.getText().toString());
                appDetails.put("AppCode", appCodeET.getText().toString());
                appDetails.put("AppName", appNameET.getText().toString());
                appDetails.put("CreatedBy", createdET.getText().toString());
                appDetails.put("MainApp", mainAppET.getText().toString());
                appDetails.put("UniqueId", uniqueET.getText().toString());
                appDetails.put("VersionName", versionET.getText().toString());

                FirebaseFirestore db = FirebaseFirestore.getInstance();
                CollectionReference uNames = db.collection("AdminNameManagement");

                uNames.document(packageEt.getText().toString()).set(appDetails)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.e(TAG, "successfully written!" + appDetails);
                                Toast.makeText(MainActivity.this, "successfully written!", Toast.LENGTH_SHORT).show();
                                adminET.setText("");
                                appCodeET.setText("");
                                appNameET.setText("");
                                createdET.setText("");
                                mainAppET.setText("");
                                uniqueET.setText("");
                                versionET.setText("");
                                packageEt.setText("");
                                dialog.dismiss();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e(TAG, "Error writing document", e);
                        Toast.makeText(MainActivity.this, "Try Again", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

            }
        });

    }

    public void addFirestore(ArrayList<AdminItems> list) {

        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("AdminNameManagement");

        for (int i = 0; i < list.size(); i++) {
            AdminItems current = list.get(i);

            String packageName = current.getPackageName();

            Map<String, Object> appDetails = new HashMap<>();
            appDetails.put("AdminName", current.getAdmin());
            appDetails.put("AppCode", current.getAppCode());
            appDetails.put("AppName", current.getAppName());
            appDetails.put("CreatedBy", current.getCreatedBy());
            appDetails.put("MainApp", current.getMainApp());
            appDetails.put("UniqueId", current.getUniqueId());
            appDetails.put("VersionName", current.getVersionName());

            uNames.document(packageName).set(appDetails)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.e(TAG, "DocumentSnapshot successfully written!" + appDetails);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e(TAG, "Error writing document", e);
                }
            });


        }

        dialog.dismiss();

    }


    public ArrayList<AdminItems> convert(JSONArray array) {
        ArrayList<AdminItems> items = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                String packageName = object.getString("Package");
                String admin = object.getString("AdminName");
                String appCode = object.getString("AppCode");
                String appName = object.getString("AppName");
                String createdBy = object.getString("CreatedBy");
                String mainApp = object.getString("MainApp");
                String uniqueId = object.getString("UniqueId");
                String versionName = object.getString("VersionName");

                items.add(new AdminItems(packageName, admin, appCode, appName, createdBy, mainApp, uniqueId, versionName));

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return items;
    }
    public List<String> getList(JSONArray array) {
        List<String> items = new ArrayList<>();
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject object = array.getJSONObject(i);
                String mainApp = object.getString("MainApp");

                items.add(mainApp);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return items;
    }

    public static String loadJson(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("adminName.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


}