package com.isu.admin;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AdminListActivity extends AppCompatActivity {
    private static final String TAG = AdminListActivity.class.getSimpleName();
    ArrayList<AdminItems> adminItems;
    AdminAdapter adapter;
    RecyclerView recyclerView;

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_list);

        recyclerView = findViewById(R.id.admin_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        showFirestore();

    }

    public void showFirestore() {
        dialog = new ProgressDialog(AdminListActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        CollectionReference uNames = db.collection("AdminNameManagement");

        uNames.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.e(TAG, "onEvent: error occured");
                }

                if (snapshots.isEmpty()) {
                    Log.e(TAG, "onEvent: empty snap");
                } else {
                    adminItems = new ArrayList<>();
                    List<DocumentSnapshot> doc = snapshots.getDocuments();
                    for (int i = 0; i < doc.size(); i++) {
                        String packageName = doc.get(i).getId();
                        Map<String, Object> appDetails = doc.get(i).getData();
                        String adminName = (String) appDetails.get("AdminName");
                        String appCode = (String) appDetails.get("AppCode");
                        String appName = (String) appDetails.get("AppName");
                        String createdBy = (String) appDetails.get("CreatedBy");
                        String mainApp = (String) appDetails.get("MainApp");
                        String uniqueId = (String) appDetails.get("UniqueId");
                        String versionName = (String) appDetails.get("VersionName");

                        adminItems.add(new AdminItems(packageName, adminName, appCode, appName, createdBy, mainApp, uniqueId, versionName));

                    }
                    adapter = new AdminAdapter(AdminListActivity.this, adminItems);
                    recyclerView.setAdapter(adapter);
                    dialog.dismiss();
                }


            }
        });


    }

}